cmake_minimum_required(VERSION 3.10)

project(zad2)

include_directories(include)

file(GLOB SOURCES "src/*.cpp" "include/*.hpp")
file(GLOB TEST_SOURCES "tests/*.cpp" "include/*.hpp")
SET(CMAKE_CXX_FLAGS "-pthread -lncurses -Wall -std=c++17 -Wextra -g")
set(EXECUTABLE_NAME "zad2")
set(TEST_NAME "tests")
add_executable(${EXECUTABLE_NAME} ${SOURCES})
add_executable(${TEST_NAME} ${TEST_SOURCES})

#set_target_properties(${EXECUTABLE_NAME} PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})

