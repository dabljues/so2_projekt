#pragma once
#include <condition_variable>
#include <map>
#include <mutex>

class station {
public:
    std::mutex mtx;
};

enum side { LEFT = 0, RIGHT = 1 };

class station1x2 {
public:
    std::mutex mutex;
    std::condition_variable cond;
    int freeSpaces = 2;
    std::map<side, int> sides = {std::pair<side, int>(side::LEFT, -1),
                                 std::pair<side, int>(side::RIGHT, -1)};

    side enter(int id) {
        std::unique_lock<std::mutex> l(mutex);
        cond.wait(l, [&]() { return freeSpaces > 0; });
        --freeSpaces;
        side s;
        if (sides[side::LEFT] == -1) {
            sides[side::LEFT] = id;
            s = side::LEFT;
        } else if (sides[side::RIGHT] == -1) {
            sides[side::RIGHT] = id;
            s = side::RIGHT;
        }
        return s;
    }

    void exit(bool left) {
        {
            std::unique_lock<std::mutex> l(mutex);
            if (left) {
                sides[side::LEFT] = -1;
            } else {
                sides[side::RIGHT] = -1;
            }
            ++freeSpaces;
        }
        cond.notify_one();
    }
};