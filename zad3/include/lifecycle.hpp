// #pragma once
// #include <workshop.hpp>

// std::vector<car *> cars;

// class lifecycle {
// public:
//     lifecycle(workshop &w_ref);
//     std::thread t;
//     std::thread update_thread;
//     workshop &w;
//     std::mt19937 rng{std::random_device{}()};
//     void life();
//     void update();
//     void new_car();
// };

// lifecycle::lifecycle(workshop &w_ref) : w(w_ref) {
//     t = std::thread(&lifecycle::life, this);
//     update_thread = std::thread(&lifecycle::update, this);
// }

// void lifecycle::life() {
//     while (true) {
//         new_car();
//         std::this_thread::sleep_for(std::chrono::seconds(5));
//     }
// }

// void lifecycle::update(ui u) {
//     while (true) {
//         w.update(cars);
//         std::this_thread::sleep_for(std::chrono::seconds(200));
//     }
// }

// void lifecycle::new_car() {
//     int phase1_machine = std::uniform_int_distribution<int>(0, 3)(rng);
//     // int phase2_machine = std::uniform_int_distribution<int>(4, 7)(rng);
//     // int phase3_machine = std::uniform_int_distribution<int>(8, 11)(rng);
//     // std::vector<int> m = {phase1_machine, phase2_machine, phase3_machine};
//     std::vector<int> m = {phase1_machine};
//     car c = car(w.car_id, m, w);
//     cars.push_back(&c);
//     c.ready = true;
//     w.car_id++;
// }
