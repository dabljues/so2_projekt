#pragma once
#include <array>
#include <atomic>
#include <chrono>
#include <mutex>
#include <point.hpp>
#include <random>
#include <station.hpp>
#include <thread>
#include <ui.hpp>

const int stations_amount = 4;

class workshop {
public:
    // ui u;
    // Stacje obsługujące dwa pojazdy
    std::array<station1x2, stations_amount> stations1x2;
    // Stacje obsługujące jeden pojazd
    std::array<station, stations_amount> stations1x1;
    // Stacje, których potrzeba 2, aby obsłużyć 1 pojazd
    std::array<station, stations_amount> stations2x1;
    std::atomic<bool> ready{false};
    std::atomic<bool> exit{false};

    int car_id = 0;
    workshop();
    ~workshop();
    // void new_car();
};

workshop::workshop() {
    // u.print_initial_state();
    // u.draw_car(0, true);
    // u.draw_car(0, false);
}

workshop::~workshop() {}

class car {
private:
    std::mt19937 rng{std::random_device{}()};
    int station_min_time;
    int station_max_time;

public:
    long long time = -1;
    int id;
    std::vector<int> machines;
    std::thread t;
    std::mutex m;
    workshop &w;
    point pos = {-1, -1};
    bool exit = false;
    std::atomic<bool> ready = false;
    std::atomic<int> state = -1;
    // True, jeśli przeszedł już wszystkie maszyny
    std::atomic<bool> finished = false;
    // Pozycja auta
    int current_machine_id = 0;
    std::atomic<bool> left = true;
    car(int id, std::vector<int> machines, workshop &w_ref,
        int station_min_time, int station_max_time);
    ~car();
    // Główny cykl "życia" auta
    void do_stuff();
    void acquire_stations(int machine_id);
};

car::car(int _id, std::vector<int> _machines, workshop &w_ref,
         int _station_min_time, int _station_max_time)
    : w(w_ref) {
    id = _id;
    machines = _machines;
    station_min_time = _station_min_time;
    station_max_time = _station_max_time;
    t = std::thread(&car::do_stuff, this);
}

car::~car() { t.join(); }

void car::do_stuff() {
    while (!w.ready) {
        std::this_thread::yield();
    }
    auto t_start = std::chrono::high_resolution_clock::now();
    while (!exit) {
        if (machines.size() > 0) {
            int machine_id = machines[0];
            acquire_stations(machine_id);
            machines.erase(machines.begin());
        } else {
            if (time == -1) {
                auto t_end = std::chrono::high_resolution_clock::now();
                time =
                    std::chrono::duration<double, std::milli>(t_end - t_start)
                        .count();
            }
            break;
        }
    }
    finished = true;
    return;
}

void car::acquire_stations(int machine_id) {
    int phase = machine_id / stations_amount;
    int number = machine_id % stations_amount;
    int sleep_time = std::uniform_int_distribution<int>(station_min_time,
                                                        station_max_time)(rng);
    int part = sleep_time / 10;
    switch (phase) {
        case 0: {
            side s = w.stations1x2[number].enter(id);
            if (exit) {
                return;
            }
            if (s == side::LEFT) {
                left = true;
            } else {
                left = false;
            }
            current_machine_id = machine_id;
            state = 0;
            for (auto i = 0; i < 10; i++)
                std::this_thread::sleep_for(std::chrono::milliseconds(part));
            w.stations1x2[number].exit(left);
            state = -1;
            break;
        }
        case 1: {
            std::lock_guard<std::mutex> guard(w.stations1x1[number].mtx);
            if (exit) {
                return;
            }
            current_machine_id = machine_id;
            state = 0;
            for (auto i = 0; i < 10; i++)
                std::this_thread::sleep_for(std::chrono::milliseconds(part));
            state = -1;
            break;
        }
        case 2: {
            int neighbour;
            if (machine_id == 3 * stations_amount - 1) {
                neighbour = number - 1;
            } else {
                neighbour = number + 1;
            }
            std::lock(w.stations2x1[number].mtx, w.stations2x1[neighbour].mtx);
            std::lock_guard<std::mutex> left_lock(w.stations2x1[number].mtx,
                                                  std::adopt_lock);
            std::lock_guard<std::mutex> right_lock(w.stations2x1[neighbour].mtx,
                                                   std::adopt_lock);
            if (exit) {
                return;
            }
            if (neighbour > number) {
                current_machine_id = machine_id;
            } else {
                current_machine_id = machine_id - 1;
            }
            left = false;
            state = 0;
            for (auto i = 0; i < 10; i++)
                std::this_thread::sleep_for(std::chrono::milliseconds(part));
            state = -1;
            break;
        }
    }
    if (exit) {
        w.exit = true;
        return;
    }
}
