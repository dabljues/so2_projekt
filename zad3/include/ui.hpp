// #pragma once

// #include <chrono>
// #include <cmath>
// #include <map>
// #include <ncurses.h>

// bool end = false;

// struct machines_offsets {
//     int phases[3][4] = {{10, 35, 60, 85}, {10, 35, 60, 85}, {10, 35, 60,
//     85}};
// } machines_offsets;

// int machine_y_low_offset = 2;
// int machine_y_high_offset = 9;
// int machine_width = 10;

// int car_width = 5;
// int car_height = 3;

// class ui {
// private:
//     int row;
//     int col;
//     int width = 5;
//     int height = 5;
//     const int max_len = 46;
//     std::mutex m;
//     std::map<int, const char *> states = {
//         std::pair<int, const char *>(-1, "waiting for station(s)"),
//         std::pair<int, const char *>(0, "being repaired")};

// public:
//     ui();
//     ~ui();
//     int x;
//     int y;
//     int phases_y[3] = {0, 0, 0};
//     // int phase1_y;
//     // int phase2_y;
//     // int phase3_y;
//     void print_initial_state();
//     void update_state(int id, const char *state, int progress);
//     // void update_particular(car *c);
//     void draw_line(point p1, int length);
//     void draw_car(int machine_id, bool left);
//     void draw_rectangle(point p1, point p2);
//     void draw_machines();
//     void update(std::vector<car *> &cars);
// };

// ui::ui() {
//     initscr();
//     noecho();
//     raw();
//     nodelay(stdscr, TRUE);
//     start_color();
//     use_default_colors();
//     curs_set(0);
//     init_pair(1, COLOR_MAGENTA, -1);
//     init_pair(2, COLOR_GREEN, -1);
//     init_pair(3, COLOR_RED, -1);
//     getmaxyx(stdscr, col, row);
//     x = row / 2;
//     phases_y[0] = col / 3;
//     phases_y[1] = phases_y[0] * 2;
//     phases_y[2] = col;
// }

// ui::~ui() { endwin(); }

// void ui::draw_line(point p1, int length) {
//     mvhline(p1.y, p1.x, 0, length);
//     refresh();
// }

// void ui::draw_rectangle(point p1, point p2) {
//     mvhline(p1.y, p1.x, 0, abs(p2.x - p1.x));
//     mvhline(p2.y, p1.x, 0, abs(p2.x - p1.x));
//     mvvline(p1.y, p1.x, 0, abs(p2.y - p1.y));
//     mvvline(p1.y, p2.x, 0, abs(p2.y - p1.y));
//     mvaddch(p1.y, p1.x, ACS_ULCORNER);
//     mvaddch(p2.y, p1.x, ACS_LLCORNER);
//     mvaddch(p1.y, p2.x, ACS_URCORNER);
//     mvaddch(p2.y, p2.x, ACS_LRCORNER);
//     refresh();
// }

// void ui::draw_car(int machine_id, bool left) {
//     int phase = machine_id / 3;
//     int number = machine_id % 4;
//     point p1;
//     point p2;
//     if (left) {
//         p1 = {x + machines_offsets.phases[phase][number] - car_width,
//               phases_y[phase] - car_height - machine_y_low_offset};
//         p2 = {x + machines_offsets.phases[phase][number],
//               phases_y[phase] - machine_y_low_offset};
//     } else {
//         p1 = {x + machines_offsets.phases[phase][number] + machine_width,
//               phases_y[phase] - car_height - machine_y_low_offset};
//         p2 = {x + machines_offsets.phases[phase][number] + machine_width +
//                   car_width,
//               phases_y[phase] - machine_y_low_offset};
//     }
//     draw_rectangle(p1, p2);
// }

// void ui::print_initial_state() {
//     draw_machines();
//     int length = x;
//     point p1;
//     for (auto p : phases_y) {
//         p1 = {x, p};
//         draw_line(p1, length);
//     }
//     refresh();
// }

// void ui::draw_machines() {
//     point p1;
//     point p2;
//     int phase_index = 0;
//     for (auto phase : phases_y) {
//         for (auto x_offset : machines_offsets.phases[phase_index]) {
//             p1 = {x + x_offset, phase - machine_y_high_offset};
//             p2 = {x + x_offset + machine_width, phase -
//             machine_y_low_offset}; draw_rectangle(p1, p2);
//         }
//         phase_index++;
//     }
//     refresh();
// }

// void ui::update(std::vector<car *> &cars) {}

// void exit() { end = true; }