#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <map>
#include <ncurses.h>
#include <stdlib.h>
#include <thread>
#include <vector>
#include <workshop.hpp>

bool end = false;
int new_car_min_time = 500;
int new_car_max_time = 1000;
int station_min_time = 700;
int station_max_time = 1000;
int machine_offset = 25;

struct machines_offsets {
    std::vector<std::vector<int>> phases;
    machines_offsets() { set_up(); }
    void set_up() {
        phases.clear();
        int initial = 10;
        for (auto i = 0; i < 3; i++) {
            std::vector<int> temp;
            for (auto i = 0; i < stations_amount; i++) {
                temp.push_back(initial + i * machine_offset);
            }
            phases.push_back(temp);
        }
    }
} machines_offsets;

int machine_y_low_offset = 2;
int machine_y_high_offset = 9;
int machine_width = 10;

int car_width = 6;
int car_height = 3;

class ui {
private:
    // Stats
    int min_wait = 9999999;
    int max_wait = 0000000;
    int cars_repaired = 0;
    int row;
    int col;
    int width = 5;
    int height = 5;
    const int max_len = 46;
    std::mutex m;
    std::map<int, const char *> states = {
        std::pair<int, const char *>(-1, "waiting for station(s)"),
        std::pair<int, const char *>(0, "being repaired")};

public:
    ui();
    int x;
    int y;
    int phases_y[3] = {0, 0, 0};
    // int phase1_y;
    // int phase2_y;
    // int phase3_y;
    void print_initial_state();
    void update_state(int id, const char *state, int progress);
    // void update_particular(car *c);
    void draw_line(point p1, int length);
    void draw_car(int machine_id, bool left, int id, bool offset);
    void draw_rectangle(point p1, point p2);
    void draw_machines();
    bool update(std::vector<car *> &cars);
    void update_particular(car *);
    void print_info(int cars_size);
    void print_color_info();
};

ui::ui() {
    initscr();
    noecho();
    raw();
    nodelay(stdscr, TRUE);
    start_color();
    use_default_colors();
    curs_set(0);
    init_pair(1, COLOR_MAGENTA, -1);
    init_pair(2, COLOR_GREEN, -1);
    init_pair(3, COLOR_CYAN, -1);
    getmaxyx(stdscr, col, row);
    x = row / 2;
    y = col / 2;
    phases_y[0] = col / 3;
    phases_y[1] = phases_y[0] * 2;
    phases_y[2] = col;
}

void ui::draw_line(point p1, int length) { mvhline(p1.y, p1.x, 0, length); }

void ui::draw_rectangle(point p1, point p2) {
    mvhline(p1.y, p1.x, 0, abs(p2.x - p1.x));
    mvhline(p2.y, p1.x, 0, abs(p2.x - p1.x));
    mvvline(p1.y, p1.x, 0, abs(p2.y - p1.y));
    mvvline(p1.y, p2.x, 0, abs(p2.y - p1.y));
    mvaddch(p1.y, p1.x, ACS_ULCORNER);
    mvaddch(p2.y, p1.x, ACS_LLCORNER);
    mvaddch(p1.y, p2.x, ACS_URCORNER);
    mvaddch(p2.y, p2.x, ACS_LRCORNER);
    // refresh();
}

void ui::draw_car(int machine_id, bool left, int id, bool offset) {
    int phase = machine_id / stations_amount;
    int number = machine_id % stations_amount;
    point p1;
    point p2;
    if (left) {
        p1 = {x + machines_offsets.phases[phase][number] - car_width,
              phases_y[phase] - car_height - machine_y_low_offset};
        p2 = {x + machines_offsets.phases[phase][number],
              phases_y[phase] - machine_y_low_offset};
    } else {
        p1 = {x + machines_offsets.phases[phase][number] + machine_width,
              phases_y[phase] - car_height - machine_y_low_offset};
        p2 = {x + machines_offsets.phases[phase][number] + machine_width +
                  car_width,
              phases_y[phase] - machine_y_low_offset};
    }
    if (offset) {
        int off = (machine_offset - car_width) / 2;
        p1.x += off;
        p2.x += off;
    }
    move(p2.y - car_height / 2, p1.x + car_width / 2);
    printw("%d", id);
    draw_rectangle(p1, p2);
    refresh();
}

void ui::print_initial_state() {
    int length = x;
    point p1;
    for (auto p : phases_y) {
        p1 = {x, p};
        attron(COLOR_PAIR(0));
        draw_line(p1, length);
        attroff(COLOR_PAIR(0));
    }
    draw_machines();
    refresh();
}

void ui::draw_machines() {
    point p1;
    point p2;
    int phase_index = 0;
    for (auto phase : phases_y) {
        for (auto x_offset : machines_offsets.phases[phase_index]) {
            p1 = {x + x_offset, phase - machine_y_high_offset};
            p2 = {x + x_offset + machine_width, phase - machine_y_low_offset};
            attron(COLOR_PAIR(phase_index + 1));
            draw_rectangle(p1, p2);
            attroff(COLOR_PAIR(phase_index + 1));
        }
        phase_index++;
    }
    refresh();
}

void ui::update_particular(car *car) {
    std::lock_guard<std::mutex> guard(m);
    int id = car->id;
    int state = car->state;
    point pos = car->pos;
    int machine_id = car->current_machine_id;
    int phase = machine_id / stations_amount;
    bool left = true;
    if (phase == 0 || phase == 2) {
        left = car->left;
    }
    if (state == -1) {
        if (pos.x == -1 && pos.y == -1) {
            pos.x = -1;
        } else {
            draw_car(machine_id, left, id, false);
        }
    } else if (state == 0) {
        bool offset = false;
        if (phase == 2) {
            offset = true;
        }
        draw_car(machine_id, left, id, offset);
    }
}

void ui::print_info(int cars_size) {
    move(y - 1, x / 4);
    printw("Cars in a workshop: %d", cars_size);
    move(y, x / 4);
    printw("Cars repaired: %d", cars_repaired);
    move(y + 1, x / 4);
    printw("Minimal full repair time: %dms", min_wait);
    move(y + 2, x / 4);
    printw("Maximal full repair time: %dms", max_wait);
}

void ui::print_color_info() {
    move(10, x / 4);
    attron(COLOR_PAIR(1));
    printw("Violet ");
    attroff(COLOR_PAIR(1));
    printw("- stations repairing two cars at once");
    move(11, x / 4);
    attron(COLOR_PAIR(2));
    printw("Green ");
    attroff(COLOR_PAIR(2));
    printw("- stations repairing one car at once");
    move(12, x / 4);
    attron(COLOR_PAIR(3));
    printw("Cyan ");
    attroff(COLOR_PAIR(3));
    printw("- there are needed two stations to repair a car");
}

bool ui::update(std::vector<car *> &cars) {
    clear();
    print_initial_state();
    print_info(cars.size());
    print_color_info();
    int c = getch();
    if (c == 113) {
        for (auto c : cars) {
            c->exit = true;
        }
        return true;
    }
    std::vector<std::thread> v;
    std::vector<int> cars_to_delete;
    int index = 0;
    for (auto c : cars) {
        if (c->finished) {
            cars_repaired++;
            if (c->time < min_wait) {
                min_wait = c->time;
            }
            if (c->time > max_wait) {
                max_wait = c->time;
            }
            cars_to_delete.push_back(index);
        } else {
            v.push_back(std::thread(&ui::update_particular, this, c));
        }
        index++;
    }

    for (auto &c : v) {
        c.join();
    }
    std::sort(cars_to_delete.rbegin(), cars_to_delete.rend());
    for (auto cdl : cars_to_delete) {
        // cars[cdl]->t.join();
        delete cars[cdl];
        cars.erase(cars.begin() + cdl);
    }
    return false;
}

ui u;

class lifecycle {
public:
    lifecycle(workshop &w_ref);
    std::thread t;
    std::thread update_thread;
    workshop &w;
    std::mt19937 rng{std::random_device{}()};
    std::vector<car *> cars;
    std::atomic<bool> exit = false;
    void life();
    void update();
    void new_car(int debug = -1);
};

lifecycle::lifecycle(workshop &w_ref) : w(w_ref) {
    t = std::thread(&lifecycle::life, this);
    new_car();
    // update_thread = std::thread(&lifecycle::update, this);
}

void lifecycle::life() {
    while (!exit) {
        int sleep_time = std::uniform_int_distribution<int>(
            new_car_min_time, new_car_max_time)(rng);
        int part = sleep_time / 10;
        for (auto i = 0; i < 10; i++)
            std::this_thread::sleep_for(std::chrono::milliseconds(part));
        new_car();
    }
    return;
}

void lifecycle::update() {
    while (!exit) {
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        exit = u.update(cars);
    }
    return;
}

void lifecycle::new_car(int debug) {
    int phase1_machine =
        std::uniform_int_distribution<int>(0, stations_amount - 1)(rng);
    int phase2_machine = std::uniform_int_distribution<int>(
        stations_amount, 2 * stations_amount - 1)(rng);
    int phase3_machine = std::uniform_int_distribution<int>(
        2 * stations_amount, 3 * stations_amount - 1)(rng);
    // std::vector<int> m = {phase1_machine, phase2_machine, phase3_machine};
    int val1 = phase2_machine;
    int val2 = phase3_machine;
    std::vector<int> m = {val1, val2};
    if (debug == -1) {
        m.insert(m.begin(), phase1_machine);
    }
    cars.push_back(new car(w.car_id, m, w, station_min_time, station_max_time));
    w.car_id++;
}

void exit() { end = true; }

void set_up_globals(bool pc) {
    if (pc) {
        return;
    } else {
        machine_offset = 15;
        car_width = 4;
        machine_width = 6;
    }
    machines_offsets.set_up();
}

int main(int argc, char **argv) {
    if (argc >= 3) {
        new_car_min_time = std::stoi(argv[1]);
        new_car_max_time = std::stoi(argv[2]);
    }
    if (argc >= 5) {
        station_min_time = std::stoi(argv[3]);
        station_max_time = std::stoi(argv[4]);
    }
    if (argc == 6) {
        if (std::string(argv[5]) == "thinkpad") {
            set_up_globals(false);
        }
    }
    move(0, 0);
    printw("Argc: %d", argc);
    u.print_initial_state();
    workshop w;
    lifecycle l(w);
    std::this_thread::sleep_for(std::chrono::seconds(1));
    w.ready = true;
    std::thread t1(&lifecycle::update, &l);
    t1.join();
    // getchar();

    for (auto car : l.cars) {
        delete car;
    }
    l.t.join();
    endwin();
    return 0;
}
