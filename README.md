# so2_projekt

## zad1

Simple exercise checking the basic understanding of threads.

## zad2 && zad2_more_threads

Dining philosophers problem.

Mainly solved with `std::lock` use, which locks two mutexes at the same moment, but only if **both** of them are guaranteed to be lockable.

Visualization provided with the use of the `ncurses` library:

![zad2](https://i.imgur.com/dr3Ju0P.gif)

## zad3

Car workshop simulation with shared resources (machines).

There are 3 types of machines, able to operate on:

* 2 cars at once

* 1 car at once

* 2 machines are needed to operate on one car

![zad3](https://i.imgur.com/UAexEn6.gif)
