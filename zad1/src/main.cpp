#include <iostream>
#include <thread>
#include <vector>
#include <string>
#include <iphilosopher.hpp>
#include <unistd.h>

int main(int argc, char** argv) {
    std::vector<std::thread> philosophers;
    if (argc == 4) {
        int numberOfThreads;
        int minTime, maxTime;
        numberOfThreads = std::stoi(argv[1]);
        minTime = std::stoi(argv[2]);
        maxTime = std::stoi(argv[3]);
        for (int i = 0; i < numberOfThreads; ++i) {
            philosophers.push_back(std::thread(&iphilosopher::doStuff, iphilosopher(i, minTime, maxTime)));
            usleep( 50000 );
        }
    } else {
       for (int i = 0; i < 5; ++i) {
            philosophers.push_back(std::thread(&iphilosopher::doStuff, iphilosopher(i)));
        } 
    }
    for (auto &p: philosophers) {
        if (p.joinable()) {
            p.join();
        }
    }
    return 0;
}