#pragma once

#include <stdio.h>
#include <random>
#include <chrono>

class iphilosopher {
private:
    int id;
    int minTime, maxTime;
    std::mt19937 rng{ std::random_device{}() };
public:
    iphilosopher(int i, int minTime=100, int maxTime=500);
    void doStuff();
};


iphilosopher::iphilosopher(int i, int min, int max) : id(i), minTime(min), maxTime(max) {}

void iphilosopher::doStuff() {
    printf("Philosopher %d started >>>>>\n", id);
    for (auto i = 0; i < 10; ++i) {
        printf("Philosopher %d is eating\n", id);
        std::this_thread::sleep_for(std::chrono::milliseconds(std::uniform_int_distribution<int>(minTime, maxTime)(rng)));
        printf("Philosopher %d is thinking\n", id);
        std::this_thread::sleep_for(std::chrono::milliseconds(std::uniform_int_distribution<int>(minTime, maxTime)(rng)));
    }
    printf("Philosopher %d ended <<<<<\n", id);
    return;
}
