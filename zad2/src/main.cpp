#include <iostream>
#include <map>
#include <ncurses.h>
#include <thread>
#include <vector>

#include <dining_philosophers.hpp>

//#define PBAR "||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBAR "##################################################"
#define LENGTH 50

std::vector<philosopher *> philosophers;
bool end = false;

class ui {
private:
    int row;
    int col;
    int x;
    int y;
    const int max_len = 46;
    std::mutex m;
    std::map<int, const char *> states = {
        std::pair<int, const char *>(-1, "waiting for table"),
        std::pair<int, const char *>(0, "thinking"),
        std::pair<int, const char *>(1, "eating"),
        std::pair<int, const char *>(2, "waiting for forks")};

public:
    ui();
    ~ui();
    void print_initial_state();
    void update_state(int id, const char *state, int progress);
    void update();
};

ui::ui() {
    initscr();
    noecho();
    raw();
    nodelay(stdscr, TRUE);
    start_color();
    use_default_colors();
    curs_set(0);
    init_pair(1, COLOR_MAGENTA, -1);
    init_pair(2, COLOR_GREEN, -1);
    init_pair(3, COLOR_RED, -1);
    getmaxyx(stdscr, col, row);
    x = row / 2 - max_len;
    y = col / 2;
}

ui::~ui() { endwin(); }

void exit() { end = true; }

void ui::update() {
    while (true) {
        int c = getch();
        if (c == 113) {
            // Wyjście z programu
            for (auto phil : philosophers) {
                phil->exit = true;
            }
            return;
        }
        for (auto phil : philosophers) {
            int id = phil->id;
            int state = phil->state;
            move(y + id - 2, 0);
            clrtoeol();
            move(y + id - 2, x);
            if (state == 2) {
                attron(COLOR_PAIR(3));
                printw("Philosopher %d is %s", id, states[state]);
            } else {
                int progress = phil->progress;
                int lpad = std::round(progress / 100.0f * LENGTH);
                int rpad = LENGTH - lpad;
                attron(COLOR_PAIR(state + 1));
                printw("Philosopher %d is %s \t\t %3d%% [%.*s%*s]", id,
                       states[state], progress, lpad, PBAR, rpad, "");
                // printw("Philosopher %d is %s, progress: %d%%", id,
                // states[state],
                //    progress);
            }
            refresh();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
}

int main() {
    dining_philosophers table;
    ui u;
    // std::vector<philosopher> philosophers;
    for (auto i = 0; i < 4; i++) {
        philosopher *p = new philosopher(i + 1, table, std::ref(table.forks[i]),
                                         std::ref(table.forks[i + 1]));
        philosophers.push_back(p);
    }
    philosophers.push_back(
        new philosopher(5, table, table.forks[4], table.forks[0]));
    // std::thread t{[&]() {}};
    std::this_thread::sleep_for(std::chrono::seconds(1));
    table.ready = true;
    std::thread t1(&ui::update, &u);
    // std::this_thread::sleep_for(std::chrono::seconds(5));
    // t.join();
    t1.join();
    for (auto p : philosophers) {
        p->t.join();
    }
    endwin();
    return 0;
}
